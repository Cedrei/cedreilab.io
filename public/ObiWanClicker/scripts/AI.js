/*
*	Just a fun side project, it isn't actually available to the player
*/

class AI {
	constructor(tapsPerSecond, goalScore) {
		this.tapsPerSecond = tapsPerSecond
		this.goalScore = goalScore
		this.finished = false

	}

	getRevenueStreamDiff(upgrade) {
		let currentRevenueStream = game.totalCps + game.tapScore * game.AI.tapsPerSecond
		let newTotalCps = game.totalCps + upgrade.cps
		let newTapScore = (newTotalCps > 12) ? newTotalCps/12 : 1
		return newTotalCps + newTapScore*game.AI.tapsPerSecond - currentRevenueStream
	}

	getMostExpensiveUpgradeCost() {
		let mostExpensiveUpgradeCost = 0
		for(let i = 0; i < game.upgrades.length; i++) {
			if (game.upgrades[i].cost > mostExpensiveUpgradeCost) {
				mostExpensiveUpgradeCost = game.upgrades[i].cost
			}
		}
		return mostExpensiveUpgradeCost
	}

	compareUpgrades(a,b) {
		let costA = a.cost
		let costB = b.cost
		let currentRevenueStream = game.totalCps + game.tapScore * game.AI.tapsPerSecond
		if (costA < costB) {
			costA -= (costB-costA)/currentRevenueStream * a.cps
		} else {
			costB -= (costA-costB)/currentRevenueStream * b.cps
		}

		let scoreA = a.cps / costA
		let scoreB = b.cps / costB

		if (scoreA < 0 || scoreB < 0) {
			return scoreA - scoreB
		}

		return scoreB - scoreA
	}

	getBestUpgrade(debug) {

		if (game.forcePoints > game.AI.goalScore && !game.AI.finished) {
			game.AI.finished = true
			console.log(new Date().getTime() - game.AI.startTime + " milliseconds")
		}


		let upgrades = JSON.parse(JSON.stringify(game.upgrades))

		upgrades.push({
			"name": "goalScore",
			"cost": game.AI.goalScore,
			"cps": 999999999999999999999999,
			"description": "You win!",
			"sound": "hellothere",
			"amount": 0
		})



		upgrades.sort(game.AI.compareUpgrades)

		if (debug) {
			console.log(upgrades)
		}

		//console.log(upgrades[0].name + " is best")

		return Math.log10(upgrades[0].cps)+1
	}

	purchaseBestUpgrades() {
		for (let i = 0; i < 12; i++) {
			let bestUpgrade = game.AI.getBestUpgrade()
			if (game.forcePoints >= game.upgrades[bestUpgrade].cost) {
				game.purchase({data: {number: bestUpgrade}})
			} else {
				break
			}
		}
	}

	startAI() {
		this.startTime = new Date().getTime()
		setInterval(game.sprites.obiwanHead.clickOnObiwan, 1000/this.tapsPerSecond)
		setInterval(this.purchaseBestUpgrades, 20)
	}
}