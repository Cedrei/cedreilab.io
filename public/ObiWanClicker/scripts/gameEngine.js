/*
*	The largest code file, in between AppEngine and AnimationEngine in abstraction level
*	Most of the actual gameplay logic is stored here
*/

class GameEngine extends AnimationEngine {
	constructor() {
		super()
		this.forcePoints = 0
		this.forcePointsMeter = $("#force-points")
		this.upgradesList = $("#upgrades")
		this.amountOfShownUpgrades = 0
		this.mousePos = {x: 0, y: 0}
		this.upgrades = [
			{
				"name": "Hello There",
				"cost": 10,
				"cps": 0.1,
				"description": "A new player? You are a bold one.",
				"sound": "hellothere",
				"amount": 0
			},
			{
				"name": "The Negotiator",
				"cost": 100,
				"cps": 1,
				"description": "Obi-Wan negotiates free Force Points for you!",
				"sound": "negotiator",
				"amount": 0
			},
			{
				"name": "Use the Force",
				"cost": 1000,
				"cps": 10,
				"description": "Don’t underestimate the Force. It gives you points!",
				"sound": "useforce",
				"amount": 0
			},			
			{
				"name": "Point of View",
				"cost": 10000,
				"cps": 100,
				"description": "From of certain point of view, this upgrade will give you infinite cookies if you play long enough.",
				"sound": "pointofview",
				"amount": 0
			},
			{
				"name": "Deal in Absolutes",
				"cost": 100000,
				"cps": 1000,
				"description": "Only sith deals in absolutes. But to get force points, you need to study all the aspects of the force!",
				"sound": "absolutes",
				"amount": 0
			},
			{
				"name": "Civilize",
				"cost": 1000000,
				"cps": 10000,
				"description": "Not having enough force points is so uncivilized. Just like blasters!",
				"sound": "uncivilized",
				"amount": 0
			},
			{
				"name": "Get Struck Down",
				"cost": 10000000,
				"cps": 100000,
				"description": "How else would you become more powerful than Vader can imagine?",
				"sound": "strikedown",
				"amount": 0
			},
			{
				"name": "High Ground",
				"cost": 100000000,
				"cps": 1000000,
				"description": "This secret technique will grant you One(1) Million force points every second! Sith lords hate it!",
				"sound": "highground",
				"amount": 0
			}
		]

		// passiveIncome and showPopup are added as animations
		// This might seem unintuitive, but since they are both functions that need to be executed each frame
		// it makes perfect sense and this is the easiest way to do it, instead of creating a complete different solution for them
		// More info about the functions were they are defined further down in this file
		this.addAnimation(this.passiveIncome, "passiveIncome")
		this.addAnimation(this.showPopup, "showPopup")

		$(document).mousemove(function(event) {
	        game.mousePos.x = event.pageX;
	        game.mousePos.y = event.pageY;
	    })
	}

	// TotalCps for the idle income defined as a getter function for convenience
	get totalCps() {
		let totalCps = 0
		for (let i = 0; i < this.upgrades.length; i++) {
			totalCps += this.upgrades[i].cps * this.upgrades[i].amount
		}
		return totalCps
	}

	// TapScore is 1/12 of the passive income per second, but never lower than 1
	get tapScore() {
		return (this.totalCps > 12) ? this.totalCps/12 : 1
	}

	// This function is for updating the force score
	// Use this function so that the meter also gets updated and not just the internal value
	changeForceScore(amount) {
		this.forcePoints += amount
		this.forcePointsMeter.text(this.fpRound(Math.floor(this.forcePoints)))
	}

	// This gets called when the obiwan head is tapped
	tap() {
		this.changeForceScore(this.tapScore)
	}

	// Title is self explanatory. Is used by the setupUpgradesList function, that loops through all the upgrades.
	insertUpgradeToList(upgradeData) {
		let amount = upgradeData.amount
		// We don't want a number until the first is bought
		// Fun fact: This wouldn't have been this easy to do in a statically typed language like Java ;)
		if (upgradeData.amount == 0)
			amount = ""

		// Each time the upgrade is bought, the price increases with 10%
		upgradeData.cost *= Math.pow(1.1, upgradeData.amount)

		// Here, the html for the button is generated into a variable
		let upgrade = "<div class='upgrade' id='upgrade-" + this.amountOfShownUpgrades + "'><div class='upgrade-name' id='upgrade-name-" + this.amountOfShownUpgrades + "'>"
		upgrade += upgradeData.name
		upgrade += "</div><div class='upgrade-cost' id='upgrade-cost-" + this.amountOfShownUpgrades + "'>"
		upgrade += this.fpRound(Math.floor(upgradeData.cost)) + " force points"
		upgrade += "</div><div class='upgrade-amount' id='upgrade-amount-" + this.amountOfShownUpgrades + "'>"
		upgrade += amount
		upgrade += "</div></div>"

		this.upgradesList.append(upgrade)
		$("#upgrade-" + this.amountOfShownUpgrades).click({number: this.amountOfShownUpgrades}, this.purchase)
		// This variable is for generating the html IDs for the upgrade buttons and their child elements
		this.amountOfShownUpgrades += 1
	}

	// Gets called when the game starts
	setupUpgradesList() {
		for (let i = 0; i < this.upgrades.length; i++) {
			this.insertUpgradeToList(this.upgrades[i])
		}
	}

	// Gets called when the player clicks an upgrade button
	// The event parameter is the click event, and contains the ID of the upgrade
	purchase(event) {
		let number = event.data.number

		// First, we have to check if we can afford the upgrade
		// We need to make a rounding here, so that the player doesn't get into a situation where it looks like they can
		// afford the upgrade but actually can't since the displayed number is rounded while the internal one is not
		// The displayed value takes precedence since the other way around could affect the user experience negatively
		if (game.forcePoints >= game.costRound(game.upgrades[number].cost)) {
			game.upgrades[number].amount += 1
			$("#upgrade-amount-"+number).text(game.upgrades[number].amount)

			// Debit the cost from the force score, needs to round here as well for the same reason as above
			game.changeForceScore(-game.costRound(game.upgrades[number].cost))
			
			// Increase the cost
			game.upgrades[number].cost *= 1.1
			// Update the text, and prettify the number
			$("#upgrade-cost-"+number).text(game.fpRound(Math.floor(game.upgrades[number].cost)) + " force points")

			// Play the upgrade's associated sound
			if (game.upgrades[number].sound != undefined) {
				game.soundEngine.play(game.upgrades[number].sound)
			}
		}
	}

	// Unlike the stock market, the flow of money here is completely linear and predictable ;)
	// Gets called every frame
	passiveIncome(delta) {
		let addedScore = 0
		// Add all passive income together, using the intenal value and not the displayed one for extra accuracy
		for (let i = 0; i < game.upgrades.length; i++) {
			addedScore += game.upgrades[i].cps * game.upgrades[i].amount
		}

		// Update the counter shwoing the force points/second
		$("#fpps").text(game.fpRound(addedScore))
		// Add the score. Remember to multiply by delta so that the player doesn't get the full one second revenue each frame!
		game.changeForceScore(addedScore * delta)
	}

	// Very similar to the previous function
	// This is called when the user logs back into the game
	// We can't use the game variable here since this is called before it is fully initialized
	// But because of a JS quirk, we can't use "this" in the previous one, since it is called on an interval
	// This function also has to display the welcome message
	idleEarnings(time) {
		let addedScore = 0
		for (let i = 0; i < this.upgrades.length; i++) {
			addedScore += this.upgrades[i].cps * this.upgrades[i].amount
		}

		// Only give 1/12th the revenue when inactive
		// It still gets really massive really quick overnight
		// Time is also measured in milliseconds here
		this.changeForceScore(addedScore * time / 12000)
		if (Math.floor(addedScore * time / 12000) > 0) {
			// Only show the welcome message if there has been revenue
			// Thus it won't appear the first time the player opens the game
			console.log("Showing")
			this.showWelcomePopup("Welcome back!<br><br> While you were away, you earned " + this.fpRound(Math.round(addedScore * time / 12000)) + " force points.")
		}
	}

	// Shows when opening the game, if the inactive revenue has been larger than 0
	showWelcomePopup(text) {
		const welcomePopup = $("#welcome-popup")
		welcomePopup.css("left", ($("html").width() / 2 - 350) + "px")
		$("#welcome-text").html(text)
		$("#welcome-popup").show()

		// Hide it automativally after 3 seconds
		setTimeout(function(){$("#welcome-popup").hide(); console.log("hiding")}, 3000)
	}

	// Show the upgrade description popup, and fill it with the appropriate data
	showPopup(delta) {
		if ($(".upgrade:hover").length != 0) {
			$("#popup").show()
			// You can't ask JS what you are hovering, instead you have to loop through everything manually and check if you are hovering it
			for (let i = 0; i < game.upgrades.length; i++) {
				if ($("#upgrade-"+i+":hover").length != 0) {
					$("#popup").css("top", $("#upgrade-"+i)[0].getBoundingClientRect().top+12 + "px")
					$("#popup-text").text(game.upgrades[i].description)
					$("#popup-earn").text("Gives "+game.fpRound(game.upgrades[i].cps)+" points/second")
				}
			}
		} else {
			// We don't hover any upgrade, so just hide the popup
			$("#popup").hide()
		}
	}

	// Prettify displays of force counts
	fpRound(n) {
		if (n < 1000) {
			// Under 1000, round to one decimal
			return Math.floor(n * 10) / 10
		} else if (n < 1000000) {
			// Under 1 million, round to whole number and also add a space between the thousands and the last three digits
			return Math.floor(n / 1000) + " " + Math.floor((n % 1000) / 100) + Math.floor((n % 100) / 10) + Math.floor(n % 10)
		} else if (n < 1000000000) {
			// Under 1 billion, round to 1/10th of a million and display as millions
			return Math.floor(n / 100000) / 10 + " million"
		} else if (n < 1000000000000) {
			// Under 1 trillion, round to 1/10th of a billion and display as billions
			return Math.floor(n / 100000000) / 10 + " billion"
		} else if (n < 1000000000000000) {
			// Under 1 quadrillion, round to 1/10th of a trillion and display as trillions
			return Math.floor(n / 100000000000) / 10 + " trillion"
		} else if (n < 1000000000000000000) {
			// Under 1 quintillion, round to 1/10th of a quadrillion and display as quadrillions
			return Math.floor(n / 100000000000000) / 10 + " quadrillion"
		} else {
			// The game really should never have to show this large numbers
			// The one exception is if the player farms like a lunatic, and then they will be able to find this little easter egg ;)
			return "Too much"
		}
	}

	// This is a slightly different rounding method
	// It is used internally for debiting costs of upgrades, and to compare the player's money to what they need
	costRound(n) {
		// It works the same as the previous function, but it returns as int instead of a prettified variable of a few different data types
		// Fun fact: That is completely reasonable in JS. While it in theory could create hard situations, it is actually a lot more intuitive
		// and as long as good preogramming practices when naming the variable and using the for sensible stuff, it isn't more dangerous than
		// driving on both sides of the road in different directions in 80 km/h or more, with nothing but a drawn line separating them ;)
		// Another difference is that it rounds to whole numbers when under 1000, instead of one decimal place.
		if (n < 1000000) { // Beneath 1 million
			return Math.floor(n)
		} else if (n < 1000000000) { // millions
			return Math.floor(n/100000)*100000
		} else if (n < 1000000000000) { // billions
			return Math.floor(n/100000000)*100000000
		} else if (n < 1000000000000000) { // trillions
			return Math.floor(n/100000000000)*100000000000
		} else if (n < 1000000000000000000) { // quadrillions+
			return Math.floor(n/100000000000000)*100000000000000
		}
	}
}