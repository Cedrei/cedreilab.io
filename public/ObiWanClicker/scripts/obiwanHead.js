/*
*	The obiwan head is so advanced that it deserves a code file of its own
*	It has the constant rotate animation, the click animation, and also needs to move when the screen is resized
*/

class ObiwanHead {
	constructor(spriteID) {
		// spriteID is the html id of the obiwan head
		this.sprite = $("#"+spriteID)
		this.currentRotation = 0
		this.scaleMultiplier = 1

		// Get and set the proper size and position for the sprite
		let size = this.getObiwanHeadScale(this)
		let position = this.getObiwanHeadPosition(size)
		this.sprite.css("height", size.y + "px")
		this.sprite.css("width", size.x + "px")
		this.sprite.css("left", position.x + "px")
		this.sprite.css("top", position.y + "px")
		this.sprite.currentRotation = 0

		// Setup the rotate animation, the resize function, and the on-click function
		game.addAnimation(this.rotateObiwan, "rotateObiwan")
		game.addResizeFunction(this.resizeObiwan)
		this.sprite.click(this.clickOnObiwan)
	}

	// Is called when the image is clicked
	// It plays the animation, and also tells the gameEngine that it has been tapped
	clickOnObiwan() {
		game.addAnimation(game.sprites.obiwanHead.obiwanBounce, "obiwanClicked", {finite: true, animTime: 0.1})
		game.tap()
	}

	// The click animation function
	obiwanBounce(time) {
		// Scale is a second degree polynomial function
		// As smallest, it is 1/40th smaller than the original size in each direction
		// (Area is about 1/20th smaller)
		game.sprites.obiwanHead.scaleMultiplier = 10 * time * time - 1 * time + 1
		game.sprites.obiwanHead.resizeObiwan()
	}

	// This function gets the default size the head should have on screen
	// It is as large as possible, under the constraint that it can only be scaled
	// equally in both dimensions and also can never be more than 40% of the width nor height
	getObiwanHeadScale(obiwanHead) {
		// Get the size of the clicking area viewport (the blue part)
		let fullX = $("#clicking-area").width()
		let fullY = $("#clicking-area").height() 

		// Get the fraction of the area it covers in each dimension
		let xPercent = fullX / 649
		let yPercent = fullY / 924
		let scale = {}

		if (xPercent < yPercent) {
			// If X is the limiting dimension, then scale it until it covers 40% of the X-dimension
			scale.x = xPercent * 0.4 * 649 * obiwanHead.scaleMultiplier
			scale.y = xPercent * 0.4 * 924 * obiwanHead.scaleMultiplier
		} else {
			// If Y is the limiting dimension, then scale it until it covers 40% of the Y-dimension
			scale.y = yPercent * 0.4 * 924 * obiwanHead.scaleMultiplier
			scale.x = yPercent * 0.4 * 649 * obiwanHead.scaleMultiplier
		}
		return scale
	}

	// Get the default position on the screen for the head
	// It is centered in the blue area
	getObiwanHeadPosition(scale) {
		// Get the dimensions of the blue area
		let fullX = $("#clicking-area").width()
		let fullY = $("#clicking-area").height()
		let position = {}

		// The pivot point is in the top left corner, so subtract the size of the head
		position.x = (fullX - scale.x) / 2
		position.y = (fullY - scale.y) / 2
		return position
	}

	// The animation function for the rotation
	rotateObiwan(delta) {
		// It rotates 3.6 degrees each second (takes 100 seconds to make a full turn)
		// 3.6*delta to the old rotation to get the new one, and then apply it
		let newRotation = game.sprites.obiwanHead.currentRotation + 3.6 * delta
		game.sprites.obiwanHead.currentRotation = newRotation
		game.sprites.obiwanHead.sprite.css("transform", `rotate(${newRotation}deg)`)
	}

	// Update size and position when screen is resized
	resizeObiwan() {
		// Get the new wanted size and position
		let size = game.sprites.obiwanHead.getObiwanHeadScale(game.sprites.obiwanHead)
		let position = game.sprites.obiwanHead.getObiwanHeadPosition(size)

		// Use those values to set the size and position with css
		game.sprites.obiwanHead.sprite.css("height", size.y + "px")
		game.sprites.obiwanHead.sprite.css("width", size.x + "px")
		game.sprites.obiwanHead.sprite.css("left", position.x + "px")
		game.sprites.obiwanHead.sprite.css("top", position.y + "px")
	}
}