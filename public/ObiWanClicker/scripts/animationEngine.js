/*
*	The most low-level functions, that are for powering the animations
*/

class AnimationEngine {
	constructor() {
		this.lastFrame = new Date().getTime() / 1000
		this.animations = {}
		this.sprites = {}
		this.resizeFunctions = []
		$(window).resize(this.loopResizeFunctions)
	}

	/* 
	*	Call this function to add in a animation function to the list
	*	An animation function will be called each frame, with a "delta" argument 
	* 	that tells it the time in seconds since the last frame
	* 	Options can include "finite" - if true it sets the animtion on a time limit
	*	and "animTime" - the time the animation will run
	*/
	addAnimation(func, name, options) {
		if (options == undefined) {
			options = {}
		}
		let animation = {run: func, finite: options.finite || false, timeLeft: options.animTime || Infinity, totalTime: options.animTime || Infinity}
		this.animations[name] = animation
	}

	// This function loops every 20 milliseconds. Each time it is called,
	// it calls all the animation functions with the delta argument
	// It also removes all finished animations
	drawLoop() {
		// Since time is checked at the start of each frame, there will be no gaps
		// A one second animation will always be one second
		let currentTime = new Date().getTime() / 1000
		let timeElapsed = currentTime - game.lastFrame
		game.lastFrame = currentTime

		for (let i in game.animations) {
			if (game.animations[i].finite) {
				// Animation is finite, a lot extra work to do
				game.animations[i].timeLeft -= timeElapsed
				if (game.animations[i].timeLeft <= 0 && game.animations[i].toBeTerminated == undefined) {
					// When time is out, we have to let the animation run to finish (when time left is 0)
					game.animations[i].timeLeft == 0
					game.animations[i].toBeTerminated = true
				} else if (game.animations[i].toBeTerminated) {
					// The toBeTerminated flag got set in the previos code block
					// The 0 time left frame has been run, so we can just delete the animation from the list
					delete game.animations[i]
					continue
				}
				game.animations[i].run(game.animations[i].totalTime - game.animations[i].timeLeft)
			} else {
				// The easy case, infinite animation so just tell the function the time elapsed
				game.animations[i].run(timeElapsed)
			}
		}
	}

	// Resize functions gets called each time the screen is resized
	// Otherwise works the same as animation functions
	addResizeFunction(func) {
		this.resizeFunctions.push(func)
	}

	// Loop through all resize functions
	// Is called when screen is resized
	loopResizeFunctions() {
		for (let i = 0; i < game.resizeFunctions.length; i++) {
			game.resizeFunctions[i]()
		}
	}
}