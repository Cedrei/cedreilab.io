/*
*	Top level functions goes here
*	Things that are built on top of the core game
*	Mostly cookie functions, but we also load the sound library here
*/

class AppEngine extends GameEngine {
	constructor() {
		super()
		this.soundEngine = new SoundEngine()
		this.loadFromCookie()
		setInterval(this.saveToCookie, 1000)

		$("#reset-button").click(this.resetProgress)

		// Experimental AI for the game, not available to the player
		//this.AI = new AI(3, Infinity)
	}

	// This function is run when the game is run
	loadFromCookie() {
		// This array serves borh for telling us the defaults, but also as a convenient list of all stored data points
		// Thus we can iterate these
		const storageDataDefaults = [
			{name: "forcePoints", default: 0}, 
			{name: "amountUpgrade0", default: 0}, 
			{name: "amountUpgrade1", default: 0}, 
			{name: "amountUpgrade2", default: 0}, 
			{name: "amountUpgrade3", default: 0}, 
			{name: "amountUpgrade4", default: 0}, 
			{name: "amountUpgrade5", default: 0}, 
			{name: "amountUpgrade6", default: 0}, 
			{name: "amountUpgrade7", default: 0},
			{name: "time", default: new Date().getTime()}
		]

		let storageData = {}

		// Loop the stored data points, and fill the storageData object with either the stored data or the dafault value
		for (let i = 0; i < storageDataDefaults.length; i++) {
			let name = storageDataDefaults[i].name
			storageData[name] = localStorage.getItem(name)
			if (storageData[name] == null) {
				storageData[name] = storageDataDefaults[i].default
			}
		}

		// Set all appropriate variables their values from the sorageDataObject
		// All localStorage data is as string, so we need to parse out the Ints
		this.forcePoints = parseInt(storageData.forcePoints)
		this.upgrades[0].amount = parseInt(storageData.amountUpgrade0)
		this.upgrades[1].amount = parseInt(storageData.amountUpgrade1)
		this.upgrades[2].amount = parseInt(storageData.amountUpgrade2)
		this.upgrades[3].amount = parseInt(storageData.amountUpgrade3)
		this.upgrades[4].amount = parseInt(storageData.amountUpgrade4)
		this.upgrades[5].amount = parseInt(storageData.amountUpgrade5)
		this.upgrades[6].amount = parseInt(storageData.amountUpgrade6)
		this.upgrades[7].amount = parseInt(storageData.amountUpgrade7)

		// We call the upgrades list setup here since it's first now we know how many of each upgrade we have
		this.setupUpgradesList()

		// Let the player have some of the points from the time they were away
		this.idleEarnings(new Date().getTime() - parseInt(storageData.time))
	}

	// This function runs every second to save the progress
	saveToCookie() {
		// Get all the data in one convenient array
		const storageDataPoints = [
			{name: "forcePoints", variable: game.forcePoints}, 
			{name: "amountUpgrade0", variable: game.upgrades[0].amount}, 
			{name: "amountUpgrade1", variable: game.upgrades[1].amount}, 
			{name: "amountUpgrade2", variable: game.upgrades[2].amount}, 
			{name: "amountUpgrade3", variable: game.upgrades[3].amount}, 
			{name: "amountUpgrade4", variable: game.upgrades[4].amount}, 
			{name: "amountUpgrade5", variable: game.upgrades[5].amount}, 
			{name: "amountUpgrade6", variable: game.upgrades[6].amount}, 
			{name: "amountUpgrade7", variable: game.upgrades[7].amount},
			{name: "time", variable: new Date().getTime()}
		]

		// Assign the array data to the appropriate localStorage variables
		for (let i = 0; i < storageDataPoints.length; i++) {
			localStorage.setItem(storageDataPoints[i].name, storageDataPoints[i].variable)
		}
	}

	// Remove all saved data
	resetProgress() {
		// Make sure it isn't an accident
		let isReallySure = confirm("Are you really sure you want to delete all progress?")
		if (!isReallySure)
			return

		// Set all values to default
		game.forcePoints = 0
		game.upgrades[0].amount = 0
		game.upgrades[1].amount = 0
		game.upgrades[2].amount = 0
		game.upgrades[3].amount = 0
		game.upgrades[4].amount = 0
		game.upgrades[5].amount = 0
		game.upgrades[6].amount = 0
		game.upgrades[7].amount = 0

		// Save the new values
		game.saveToCookie()

		// Reload the page for extra flavour (and also save us from potential bugs)
		location.reload()
	}
}