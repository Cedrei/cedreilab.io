/*
*	This file is an abstraction level for the sound library
*	The soundmanager library, that is used for this game, can play sounds in either HTML5
*	or Flash Player, to maximize browser support (HTML5 is prefered)
*/

class SoundEngine {
	constructor() {
		this.setupSoundmanager()
		this.sounds = {
			"absolutes": "chosenone.mp3",
			"negotiator": "negot.mp3",
			"hellothere": "obi-wan-hello-there.mp3",
			"strikedown": "strike.mp3",
			"pointofview": "truth-2.mp3",
			"useforce": "useforce.mp3",
			"uncivilized": "So uncivilized.mp3",
			"highground": "high-ground.mp3"
		}
	}

	// Load the library, called from constructor
	setupSoundmanager() {
		soundManager.setup({
		  	url: '/scripts/lib/swf/',
		  	flashVersion: 9,
		  	preferFlash: false, // Prefer 100% HTML5 mode, where both supported
		  	ontimeout: function() {
		  		// Print error if it fails to load
		    	console.log('SM2 init failed!');
		  	},
		  	defaultOptions: {
		    	// Set global default volume for all sound objects
		    	volume: 33
		  	}
		})
	}

	// Function to play a sound, it also stops the previous sound
	play(name) {
		soundManager.stopAll()
		soundManager.createSound({
			id: name,
		    url: 'sounds/'+this.sounds[name]
	    }).play()
	}
}